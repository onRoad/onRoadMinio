# Check if there's an active container
	echo "Checking if exists a onRoadMinio1 container..."
	DOCKER_CONTAINER_ONROAD_MINIO1_ID=$(docker ps | grep "onRoadMinio1" | sed -e 's/^\(.\{12\}\).*/\1/')

	# Check if there is an active container
	if [ ! -z "$DOCKER_CONTAINER_ONROAD_MINIO1_ID" ]; then
		echo "There are active containers. Deleting..."
		docker rm -f $(docker ps | grep "onRoadMinio1" | sed -e 's/^\(.\{12\}\).*/\1/')
	fi

echo "Creating new containers..."
docker-compose up -d